// @Author maotao
// Created by tao on 24-9-17.
// @Desc 制定规则

#include <Role.h>
#include <Block.h>
#include <main.h>
#include <Game.h>
#include <oled.h>
#include <stdio.h>

unsigned int score = 0;
unsigned int scoreScale = 1;
char scoreText[SCORE_TEXT_SIZE_DEFAULT];

Bool isDead(const Role *role, const Block *block) {
    int x1 = block->x + block->w, x2 = role->x + role->w, y1 = role->y + role->h;
    if (block->y < y1 && block->x < x2) {
        if (x1 < role->x)
            return false;
        return true;
    }
    return false;
}


Bool autoScore(const Role *role, const Block *block) {
    if (block->x + block->w == role->x - 1) {
        score += scoreScale;
        return true;
    }
    return false;
}

void OLED_DrawGameOver() {
    OLED_PrintASCIIString(30, 30, "GAME OVER!", &afont16x8, OLED_COLOR_NORMAL);
}

void OLED_DrawScoreText() {
    sprintf(scoreText, "score: %d", score);
    OLED_PrintASCIIString(SCORE_TEXT_X_INIT, SCORE_TEXT_Y_INIT, scoreText, &afont8x6, OLED_COLOR_NORMAL);
}

