// @Author maotao
// Created by tao on 24-9-16.
//

#include <Role.h>
#include <stdlib.h>
#include <oled.h>

static unsigned int _role_speed_cnf = 1;

void role_iterator(Role *role) {
    role->y += _role_speed_cnf * role->vector;

    if (role->y <= ROLE_Y_MAX) {
        role->vector = drop;
    } else if (role->y > ROLE_Y_INIT) {
        role->vector = hold;
        role->status = stand;
    }
    role->time += 1;
    if (role->time % 10 < 5) {
        role->image = role->image1;
    } else {
        role->image = role->image2;
    }
}

void role_jump(Role *role) {
    if (role->status == stand) {
        role->status = jump;
        role->vector = raise;
    }
}

void OLED_DrawRole(const Role *role) {
    OLED_DrawImage(role->x, role->y, role->image, OLED_COLOR_NORMAL);
}

