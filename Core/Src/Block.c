//
// Created by tao on 24-9-17.
//

#include "../Inc/Block.h"

#include <oled.h>
#include <stdlib.h>

static unsigned int _block_speed_cnf = 1;

Bool isBlockReBuild(Block *block) {
    int x = block->x + block->w;
    if( x < BLOCK_X_MIN) {
        return true;
    }
    return false;
}
void block_iterator(Block *block) {
    block->x -= _block_speed_cnf;
    block->time = block->time % block->img_size;
    if (isBlockReBuild(block)) {
        block->x = BLOCK_X_INIT;
    }
}


void OLED_DrawBlock(const Block *block) {
    const Image *img = block->images[block->time];
    OLED_DrawImage(block->x, block->y, img, OLED_COLOR_NORMAL);
}

//
// Bool randomIsBuildBlock(const int prev_x) {
//     if(prev_x > BLOCK_BUILD_MIN)
//         return false;
//
//     int seed = rand()%10;
//
//     return true;
// }