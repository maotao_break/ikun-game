//
// Created by tao on 24-9-17.
//

#ifndef GAME_H
#define GAME_H
#include <main.h>
#include <Block.h>
#include <Role.h>

#define SCORE_TEXT_SIZE_DEFAULT 10
#define SCORE_TEXT_X_INIT 75
#define SCORE_TEXT_Y_INIT 0

Bool isDead(const Role *role, const Block *block);

void OLED_DrawGameOver();

Bool autoScore(const Role *role, const Block *block);

void OLED_DrawScoreText();

extern unsigned int score;
extern unsigned int scoreScale;
#endif //GAME_H
