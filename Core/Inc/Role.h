//
// Created by tao on 24-9-16.
//

#ifndef ROLE_H
#define ROLE_H

#include <font.h>

#define ROLE_X_INIT 10
#define ROLE_Y_INIT 31
#define ROLE_Y_MAX 1

typedef enum _role_vector {
    raise = -1,
    hold = 0,
    drop = 1,
} RoleVector;

typedef enum _role_status {
    stand = 0,
    jump = 1
} RoleStatus;

typedef struct _role {
    unsigned int w;
    unsigned int h;
    unsigned int x;
    unsigned int y;
    unsigned int time;
    const Image *image;
    const Image *image1;
    const Image *image2;
    RoleStatus status;
    RoleVector vector;
} Role;

void role_iterator(Role *role);


void role_jump(Role *role);

Role *Role_Init(const unsigned int x, const unsigned int y, const Image *image, const Image *move_image);

void OLED_DrawRole(const Role *role);
#endif //ROLE_H
