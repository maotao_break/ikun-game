// @Author maotao
// Created by tao on 24-9-17.
//

#ifndef BLOCK_H
#define BLOCK_H

#include <font.h>

#define BLOCK_X_INIT 128
#define BLOCK_Y_INIT 49
#define BLOCK_X_MIN -20
#define BLOCK_BUILD_MIN 50
#define BLOCK_BUILD_MAX 128

typedef struct _block {
    unsigned int w;
    unsigned int h;
    unsigned int x;
    unsigned int y;
    const Image *images[4];
    unsigned int img_size;
    unsigned int time;
} Block;

void block_iterator(Block *block);

void OLED_DrawBlock(const Block *block);

#endif //BLOCK_H
